﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class vb_anim_five : MonoBehaviour
{
    public GameObject vbBtnObj;
    public Animator objAni;
    // Start is called before the first frame update
    void Start()
    {
        vbBtnObj = GameObject.Find("VirtualButtonFive");
        vbBtnObj.GetComponent<VirtualButtonBehaviour>().RegisterOnButtonPressed(OnButtonPressed);
        vbBtnObj.GetComponent<VirtualButtonBehaviour>().RegisterOnButtonReleased(OnButtonReleased);
        
        objAni.GetComponent<Animator>();
    }

    public void OnButtonPressed(VirtualButtonBehaviour vb){
        objAni.Play("rotation_animation_five");
        Debug.Log("BTN PRESSED");
    } 

    public void OnButtonReleased(VirtualButtonBehaviour vb){
        objAni.Play("none");
        Debug.Log("BTN RELEASED");
    } 

    // Update is called once per frame
    void Update()
    {
        
    }
}
