﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class vb_anim_four : MonoBehaviour
{
    public GameObject vbBtnObj;
    public Animator objAni;
    // Start is called before the first frame update
    void Start()
    {
        vbBtnObj = GameObject.Find("VirtualButtonFour");
        vbBtnObj.GetComponent<VirtualButtonBehaviour>().RegisterOnButtonPressed(OnButtonPressed);
        vbBtnObj.GetComponent<VirtualButtonBehaviour>().RegisterOnButtonReleased(OnButtonReleased);
        
        objAni.GetComponent<Animator>();
    }

    public void OnButtonPressed(VirtualButtonBehaviour vb){
        objAni.Play("rotation_animation_four");
        Debug.Log("BTN PRESSED");
    } 

    public void OnButtonReleased(VirtualButtonBehaviour vb){
        objAni.Play("none");
        Debug.Log("BTN RELEASED");
    } 

    // Update is called once per frame
    void Update()
    {
        
    }
}
